<?php

function saml_sso_admin_settings(){
	$form = array(
		'sp_idp_tabs' => array(
	    	'#type' => 'vertical_tabs',
		),

		'saml_sso_sp_settings' => array(
			'#title' => 'Service Provider Settings',
			'#type' => 'fieldset',
			'#group' => 'sp_idp_tabs',
			'#tree' => FALSE,
			'#collapsible' => true, 
		
			'saml_sso_sp_certificate' => array(
				'#title' => t('SAML Service provider certificate'),
				'#type' => 'textarea',
				'#default_value' => variable_get('saml_sso_sp_certificate')
			),

			'saml_sso_sp_acs' => array(
				'#title' => t('Service Provider assertion consumer URL'),
				'#type' => 'textfield',
				'#default_value' => variable_get('saml_sso_sp_acs')
			),

			'saml_sso_sp_slo' => array(
				'#title' => t('Service Provider single logout URL'),
				'#type' => 'textfield',
				'#default_value' => variable_get('saml_sso_sp_slo')
			),
		),

		'saml_sso_idp_settings' => array(
			'#title' => 'Identity Provider Settings',
			'#type' => 'fieldset',
			'#group' => 'sp_idp_tabs',
			'#collapsible' => true, 

			'saml_sso_idp_issuer' => array(
				'#title' => t('SAML Identity Provider Issuer'),
				'#type' => 'textfield',
				'#default_value' => variable_get('saml_sso_idp_issuer')
			),

			'saml_sso_idp_key' => array(
				'#title' => t('SAML Identity Provider Private Key'),
				'#type' => 'textarea',
				'#default_value' => variable_get('saml_sso_idp_key')
			),

			'saml_sso_idp_validate_requests' => array(
				'#title' => t('Validate incoming requests'),
				'#type' => 'checkbox',
				'#default_value' => variable_get('saml_sso_idp_validate_requests')
			),
		)			
	);

	return system_settings_form($form);
}

?>