<?php

module_load_include('inc', 'saml_sso', 'session');
module_load_include('inc', 'saml_sso', 'metadata');

function send_saml_logout_request($sp){
}

function send_saml_logout_response($sp, $request, $sloUrl){

	$lr = new SAML2_LogoutResponse();

	$cfg = SamlMetadata::getInstance();

	$lr->setIssuer($cfg->getIdpIssuer());
	$lr->setDestination($sloUrl);

	$lr->setInResponseTo($request->getId());
	$lr->setRelayState($request->getRelayState());

	$lr->setStatus(array('Code' => SAML2_Const::STATUS_SUCCESS));

	$lr->setSignatureKey($cfg->getIdpKey());

	$binding = new SAML2_HTTPRedirect();
	$binding->send($lr);
}

function saml_sso_logout_callback(){
global $user;

	$message = receiveSamlMessage();

	if (empty($message))
		return;

	$session = SamlSsoSessionStorage::getSession($user->sid);

	if ($message instanceof SAML2_LogoutRequest){
		if ($session){
			if ($session->getLogoutSp() != NULL) /// We're processing a prior logout request
				return;                                /// and not taking any new requests to do so

			$session->initLogout($request->issuer);
		}
	}
	else
	if ($message instanceof SAML2_LogoutResponse){
		$logoutSp = $session->getLogoutSp();

		if (empty($logoutSp)) /// There are no outstanding logout requests
			return;           /// in processing. This response is unsolicited and invalid
	}

	if ($session)
		$sp = $session->nextLogoutSp();

	if (isset($sp)){
		send_saml_logout_request($sp);
		return;
	}

	watchdog('user', 'Session closed for %name.', array('%name' => $user->name));

	module_invoke_all('user_logout', $user);

	// Destroy the current session, and reset $user to the anonymous user.
	session_destroy();

	if ($sp)
		$sloUrl = $sp->getSloUrl();

	if (empty($sloUrl)){
		$metadata = SamlMetadata::getInstance();
		$sloUrl = $metadata->getSpSingleLogoutUrl();
	}
	send_saml_logout_response($sp, $message, $sloUrl);
}

?>