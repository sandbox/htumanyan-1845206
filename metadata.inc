<?php

class SamlMetadata extends SimpleSAML_Configuration{
	protected static $_instance;

	public function __construct() {
    }

	public static function getInstance(){
		if (empty(self::$_instance))
			self::$_instance = new SamlMetadata();

		return self::$_instance;
	}

	public function getString($name, $default = self::REQUIRED_OPTION) {
		if ($name == 'AssertionConsumerService')
			return variable_get('saml_sso_sp_acs');

		if ($name == 'certData')
			return variable_get('saml_sso_sp_certificate');
    }

	public function getIdpKey(){
		$key = variable_get('saml_sso_idp_key');
		$privateKey = new XMLSecurityKey(XMLSecurityKey::RSA_SHA1, array('type' => 'private'));
		$privateKey->loadKey($key, FALSE);
		return $privateKey;
	}

	public function getBoolean($name, $default = self::REQUIRED_OPTION){
		if ($name == 'validate.logout' ||
			$name == 'validate.authnrequest' ||
			$name == 'redirect.validate')
		return variable_get('saml_sso_idp_validate_requests') == 1;
    }

	public function hasValue($name) {
		if ($name == 'certData'){
			$data = $this->getString('certData');
			return isset($data);
		}
		return FALSE;
	}

	public function getSpSingleLogoutBinding(){
		return 'Redirect';
	}

	public function getSpSingleLogoutUrl(){
		return variable_get('saml_sso_sp_slo');
	}

	public function getIdpIssuer(){
		return variable_get('saml_sso_idp_issuer');
	}
}

?>