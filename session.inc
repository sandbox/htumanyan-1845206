<?php

class SamlSp{
	protected $_issuer;
	protected $_acsUrl;
	protected $_sloUrl;

	public function __construct($issuer, $acsUrl, $sloUrl){
		$this->issuer = $issuer;
		$this->_acsUrl = acsUrl;
		$this->_sloUrl = sloUrl;	
	}

	public function getIssuer(){return $this->_issuer;}
//	public function getAcsUrl(){return $this->_acsUrl;}
	public function getSloUrl(){return $this->sloUrl;}
}

class SamlSsoSession{
	protected $_logoutSp;
	protected $_participants;

	public function __construct(){
		$this->_logoutSp = NULL;
		$this->_participants = array();
	}

	public function initLogout($spIssuer){
		if ($this->_logoutSp != NULL)
			return;

		foreach($this->_participants as $issuer => $sp){
			if ($issuer == $spIssuer){
				$this->_logoutSp = $sp;
				unset($this->_participants[$issuer]);
				break;
			}
		}
	}

	public function getLogoutSp(){
		return $this->_logoutSp;
	}

	public function nextLogoutSp(){
		$sp = array_shift($this->_participants);
		return $sp;
	}

	public function addSp($sp){
		$this->_participants[$sp->getIssuer()] = $sp;
	}
}

define ('SAML_SSO_CACHE_CID', 'SAML_SSO_CACHE');

class SamlSsoSessionStorage{
	protected static $_stack;

	protected static function _getStack(){
		if (empty(self::$_stack))
			self::$_stack = cache_get(SAML_SSO_CACHE_CID)->data;

		if (empty(self::$_stack))
			self::_setStack(array());

		return self::$_stack;
	}

	protected static function _setStack($stack){
		self::$_stack = $stack;
		cache_set(SAML_SSO_CACHE_CID, self::$_stack);
	}


	public static function getSession($sid){
		$stack = self::_getStack();
		if (isset($stack[$sid]))
			return $stack[$sid];

		return NULL;
	}	

	public static function saveSession($sid, $session){
		$stack = self::_getStack();
		$stack[$sid] = $session;
		$self::_setStack($stack);
	}
}

?>