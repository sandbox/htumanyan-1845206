<?php

module_load_include('inc', 'saml_sso', 'metadata');

function send_saml2_auth_response($state, $cfg){
global $user;
	$currentTime = time();
	$drupalSessionParams = session_get_cookie_params();

	$a = new SAML2_Assertion();
	$a->setIssuer($cfg->getIdpIssuer());
	$a->setValidAudiences(array($state['saml_request_issuer']));

	$a->setNotBefore($currentTime - 30);
	$a->setSessionNotOnOrAfter($user->login + $drupalSessionParams['lifetime']);

	$a->setAuthnContext(SAML2_Const::AC_PASSWORD);

	$assertionLifeEnd = $currentTime + 300; /// 5 minutes life time
	$a->setNotOnOrAfter($assertionLifeEnd); 

	$sc = new SAML2_XML_saml_SubjectConfirmation();
	$sc->SubjectConfirmationData = new SAML2_XML_saml_SubjectConfirmationData();

	$sc->SubjectConfirmationData->NotOnOrAfter = $assertionLifeEnd;
	$sc->SubjectConfirmationData->Recipient = $state['saml_acs_url'];
	$sc->SubjectConfirmationData->InResponseTo = $state['saml_request_id'];
	$sc->Method = SAML2_Const::CM_BEARER;

	$a->setSubjectConfirmation(array($sc));

/*	if (isset($state['saml_request_name_id_policy']))
		$nameIdPolicy = $state['saml_request_name_id_policy'];

	if (isset($nameIdPolicy) && isset($nameIdPolicy['Format'])) {
		$nameIdFormat = $nameIdPolicy['Format'];
	} else {
		$nameIdFormat = 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient';
	}*/

	$nameIdFormat = 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient';

	$spNameQualifier = $cfg->getIdpIssuer();
	$nameId = array(
		'Format' => $nameIdFormat,
		'Value' => $user->name,
		'SPNameQualifier' => $spNameQualifier,
	);

	$a->setNameId($nameId);

	$attributes = array(
		'urn:oid:2.5.4.3' => array($user->name), //cn
		'persistent-id' => array($user->name),
//		'uid' => array($user->uid),
		'mail' => array($user->mail),
		'roles' => $user->roles
	);

	$a->setAttributeNameFormat('urn:oasis:names:tc:SAML:2.0:attrname-format:uri');
	$a->setAttributes($attributes);

	$r = new SAML2_Response();
	$r->setIssuer($cfg->getIdpIssuer());
	$r->setDestination($state['saml_acs_url']);
	$r->setInResponseTo($state['saml_request_id']);
	$r->setRelayState($state['saml_relay_state']);

	$r->setAssertions(array($a));

	$r->setSignatureKey($cfg->getIdpKey());

	$binding = SAML2_Binding::getBinding($state['saml_binding']);
	$binding->send($r);
}

function request_to_state($samlRequest){
	return array(
		'saml_acs_url' => $samlRequest->getAssertionConsumerServiceURL(),
		'saml_binding'=> $samlRequest->getProtocolBinding(),
		'saml_relay_state' => $samlRequest->getRelayState(),
		'saml_request_id' => $samlRequest->getId(),
		'saml_request_issuer' =>$samlRequest->getIssuer()
	);
}


function saml_sso_login_callback(){
	global $user;

	try{	
	  $binding = SAML2_Binding::getCurrentBinding();

	  if (isset($binding))
		$request = $binding->receive();
	  else
		$msg = t('Unable to find matching SAML binding');

	  if (isset($request)){
		$metadata = SamlMetadata::getInstance();

		sspmod_saml_Message::validateMessage($metadata, $metadata, $request);

		if (isset($user) && is_object($user) && $user->uid != 0){
	   	  send_saml2_auth_response(request_to_state($request), $metadata, $binding, $user);
		  exit;
		}
	  }else{
	  	$msg = t('Invalid request - No SAML Messages');
	  }
    }catch(Exception $e){
		$msg = t('Failed to process SSO request: ').$e->getMessage();
    }	

	drupal_set_message($msg, 'error');
	return drupal_get_form('user_login');
}

function saml_sso_form_user_login_alter(&$form, &$form_state){
	try{	
	  $binding = SAML2_Binding::getCurrentBinding();

	  if (isset($binding)){
		$samlRequest = $binding->receive();

		if (isset($samlRequest)){		  
		  $metadata = SamlMetadata::getInstance();
		  sspmod_saml_Message::validateMessage($metadata, $metadata, $samlRequest);
	    }
	  }
    }catch(Exception $e){
		$samlRequest = NULL;
		drupal_set_message(t('Failed to process SSO request: ').$e->getMessage(), 'error');
    }	

	if (empty($samlRequest))
		return;

	$form['saml_acs_url'] = array(
		'#type' => 'hidden',
		'#value' => $samlRequest->getAssertionConsumerServiceURL()
	);

	$form['saml_binding'] = array(
		'#type' => 'hidden',
		'#value' => $samlRequest->getProtocolBinding()
	);

	$form['saml_relay_state'] = array(
		'#type' => 'hidden',
		'#value' => $samlRequest->getRelayState()
	);

	$form['saml_request_id'] = array(
		'#type' => 'hidden',
		'#value' => $samlRequest->getId()
	);

	$form['saml_request_issuer'] = array(
		'#type' => 'hidden',
		'#value' => $samlRequest->getIssuer()
	);

	$form['#submit'][] = 'process_saml_login';
}

function saml_sso_user_login($form_state){
global $user;
	$state = array();

	if ($form_state['form_id'] == 'user_pass_reset'){
		return; // Avoid interference with certain forms, namelyy password reset - more may be added.
	}

	if (empty($form_state['input']))
		return;

	if (empty($user) || !$user->uid)
		return;

	foreach(array('saml_acs_url', 'saml_binding', 'saml_relay_state', 'saml_request_id', 'saml_request_issuer') as $id)
		$state[$id] = $form_state['input'][$id];

	$metadata = SamlMetadata::getInstance();

	try{
		send_saml2_auth_response($state, $metadata);
	}catch(Exception $e){
		drupal_set_message(t('Single Sign-On Failed').$e->getMessage(), 'error');
	}
}


?>